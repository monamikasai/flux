var React       = require('react');
var TodoActions = require('../actions/TodoActions');

module.exports = React.createClass({
  handleDestroy: function(e) {
    e.preventDefault();

    TodoActions.destroy(this.props.id);
  },

  getInitialState: function() {
    return{
      done: false
    };
  },
 
  onDone: function(){
    this.setState({done: true});
  },
    
  render: function(){
    if (this.state.done){
      return(
        <div className="todo">
          <s className="name">{this.props.children}</s>
          <s className="date">{this.props.created_at}</s>
          <button onClick={this.handleDestroy}>削除</button>
        </div>
      )
    }else{
      return(
        <div className="todo">
          <input type="checkbox" ref="chk" unchecked="unchecked" onClick={this.onDone}/>
          <span className="name">{this.props.children}</span>
          <span className="date">{this.props.created_at}</span>
          <button onClick={this.handleDestroy}>削除</button>
        </div>
      )
    }
  } 
});
